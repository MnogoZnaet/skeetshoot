﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class UIReadyToShootTimer : MonoBehaviour
{
    [Inject]
    private IWeaponControl weaponControl;
    // Start is called before the first frame update
    public Image image;

    void Start()
    {
        image = this.GetComponent<Image>();
        image.fillAmount = 0f;

        weaponControl.StartTrackingEvent += StartTracking;
        weaponControl.TrackingEvent += Tracking;
        weaponControl.LostTrackingEvent += LostTracking;
    }

    void StartTracking(float time)
    {
        Debug.Log(time);
    }

    void LostTracking(float time)
    {
        image.fillAmount = 0f;
    }

    void Tracking(float value)
    {
        image.fillAmount = value;
    }
}
