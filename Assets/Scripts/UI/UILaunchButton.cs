﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class UILaunchButton : MonoBehaviour
{
    [Inject]
    ISkeetLauncherControl skeetLauncherControl;

    [SerializeField] private Image image;
    [SerializeField] private Button button;
    [SerializeField] private Text text;

    // Start is called before the first frame update
    void Start()
    {
        skeetLauncherControl.ReadyEvent += SkeetLauncherReady;
        skeetLauncherControl.LaunchEvent += SkeetLauncherLaunched;
    }

    public void OnPointerDown()
    {
        skeetLauncherControl.Launch(); 
    }

    void EnableButton(bool value)
    {
        image.enabled = value;
        button.enabled = value;
        text.enabled = value;
    }

    private void SkeetLauncherReady()
    {
        EnableButton(true);
    }

    private void SkeetLauncherLaunched()
    {
        EnableButton(false);
    }
}
