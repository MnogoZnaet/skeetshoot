﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class GameInstaller : MonoInstaller
{
    [SerializeField] private GameObject skeetLaunchController;
    [SerializeField] private GameObject inputController;
    [SerializeField] private GameObject cameraController;
    [SerializeField] private GameObject weaponController;

    public override void InstallBindings()
    {
        Container.Bind<ISkeetLauncherControl>().FromInstance(skeetLaunchController.GetComponent<ISkeetLauncherControl>()).AsSingle().NonLazy();
        Container.Bind<IInputContoller>().FromInstance(inputController.GetComponent<IInputContoller>()).AsSingle().NonLazy();
        Container.Bind<ICameraController>().FromInstance(cameraController.GetComponent<ICameraController>()).AsSingle().NonLazy();
        Container.Bind<IWeaponControl>().FromInstance(weaponController.GetComponent<IWeaponControl>()).AsSingle().NonLazy();
    }
}
