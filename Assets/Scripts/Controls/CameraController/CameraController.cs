﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class CameraController : MonoBehaviour, ICameraController
{
    [Inject]
    private IInputContoller _inputController;

    [SerializeField] private GameObject cameraRigYAxis;
    [SerializeField] private GameObject cameraRigXAxis;
    [SerializeField] private Vector2 moveVector;

    private Vector2 rotation =
    new Vector2(0, 0);
    public float mouseLookSpeed = 3.0f;
    public float minXValue = -20.0f;
    public float maxXValue = 20.0f;

    public void Start()
    {
        _inputController.MoveDirectionEvent += SetMoveVector;
    }

    public void SetMoveVector(Vector2 moveVector)
    {
        this.moveVector = moveVector;
    }

    public void LateUpdate()
    {
        if (moveVector.magnitude > 0f)
        {
            //Mouse look
            //X input
            rotation.y = moveVector.x * 20f;
            //Y input
            rotation.x = -moveVector.y * 20f;
            //Rotate on y axis based on mouse look speed
            cameraRigYAxis.transform.eulerAngles = new Vector2
                (0, rotation.y) * mouseLookSpeed;
            //Clamp rotation on x axis to min and max values
            rotation.x = Mathf.Clamp(rotation.x, minXValue, maxXValue);
            //Rotate arms on the x axis
            cameraRigXAxis.transform.localRotation = Quaternion.Euler
                (rotation.x * mouseLookSpeed, 0, 0);

        }
    }
}
