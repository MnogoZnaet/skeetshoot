﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISkeetLauncherControl 
{
    event Action LaunchEvent;
    event Action ReadyEvent;

    void Launch();
    void Ready();
}
