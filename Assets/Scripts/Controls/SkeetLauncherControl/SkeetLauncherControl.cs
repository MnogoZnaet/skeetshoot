﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkeetLauncherControl : MonoBehaviour, ISkeetLauncherControl
{
    public event Action LaunchEvent;
    public event Action ReadyEvent;

    [SerializeField] private List<GameObject> projectileActors;
    private List<IProjectileActor> actors = new List<IProjectileActor>();

    void Start()
    {
        foreach (var actor in projectileActors)
        {
            actors.Add(actor.GetComponent<IProjectileActor>());
        }

        Ready();
    }

    IEnumerator LaunchCoroutine()
    {
        yield return new WaitForSeconds(1f);
        int n = UnityEngine.Random.Range(0, 2);
        IProjectile projectile = actors[n].Fire();
        projectile.DestroyedEvent += Ready;
        yield break;
    }

    public void Launch()
    {
        if (LaunchEvent != null)
        {
            LaunchEvent();
        }

        StartCoroutine(LaunchCoroutine());
    }

    public void Ready()
    {
        if (ReadyEvent != null)
        {
            ReadyEvent();
        }
    }
}
