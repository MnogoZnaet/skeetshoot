﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlayerInput
{
    Vector2 GetDirection(Vector2 newPos, Vector2 previousPos);
}
