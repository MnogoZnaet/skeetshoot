﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class InputInfo
{
    internal bool mouseButtonDown;
    internal bool mouseButton;
    internal bool mouseButtonUp;

    internal Vector2 inputMousePosition;

    public InputInfo(bool mouseButtonDown, bool mouseButton, bool mouseButtonUp, Vector2 inputMousePosition)
    {
        this.mouseButtonDown = mouseButtonDown;
        this.mouseButton = mouseButton;
        this.mouseButtonUp = mouseButtonUp;
        this.inputMousePosition = inputMousePosition;
    }
}

public class InputController : MonoBehaviour, IInputContoller
{

    private Vector2 virtualJoystickVector;
    private Vector2 touchPos;

    private IPlayerInput playerInput;

    public event Action<Vector2> MoveDirectionEvent;

    private void Start()
    {      
      playerInput = this.GetComponent<EightInput>();
    }

    public Vector2 GetDirection()
    {
        //Debug.Log(virtualJoystickVector);
        return virtualJoystickVector;
    }

    void Update ()
    {
        if (Input.GetMouseButtonDown(0))
        {
            touchPos = Input.mousePosition;
        }
        if (Input.GetMouseButton(0))
        {
            Vector2 newTouchPos = Input.mousePosition;
           
            if((newTouchPos - touchPos).magnitude != 0)
            {
                virtualJoystickVector = playerInput.GetDirection(newTouchPos, touchPos);
            }
            touchPos = newTouchPos;
        }
        else if (Input.GetMouseButtonUp(0))
        {           
            virtualJoystickVector = Vector2.zero;
        }

        if (MoveDirectionEvent != null)
        {
            MoveDirectionEvent.Invoke(GetDirection());
        }
    }

     
}
