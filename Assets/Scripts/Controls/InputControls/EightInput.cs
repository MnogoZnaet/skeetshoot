﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EightInput : MonoBehaviour, IPlayerInput
{
    Vector2 virtualJoystickVector;
    private float sensitive = 1000f;

    public Vector2 GetDirection(Vector2 newTouchPos, Vector2 touchPos)
    {
        Vector2 deltaTouch = newTouchPos - touchPos;
        float parameter = Mathf.InverseLerp(0f, sensitive, deltaTouch.magnitude);
        virtualJoystickVector = Vector2.Lerp(virtualJoystickVector, deltaTouch.normalized, parameter);
        if (virtualJoystickVector.magnitude > 1f)
            virtualJoystickVector = virtualJoystickVector.normalized;

        return virtualJoystickVector;
    }
}
