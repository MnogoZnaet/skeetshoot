﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponControl : MonoBehaviour, IWeaponControl
{
    public event Action ShootEvent;
    public event Action ReloadEvent;
    public event Action<float> StartTrackingEvent;
    public event Action<float> TrackingEvent;
    public event Action<float> LostTrackingEvent;

    [SerializeField] private List<RectTransform> raycastPoints;
    private List<Ray> aimRays = new List<Ray>();

    [SerializeField] private float[] trackingToShootTime = new float[5] {0.25f, 0.5f, 0.75f, 1f, 2f};

    private float trackingTime;
    private IProjectile trackingObject;
    private float readyToShootTime;


    public void Start()
    {

    }

    public void Reload()
    {
        if (ReloadEvent != null)
        {
            ReloadEvent();
        }
    }

    public void Shoot()
    {
        if (ShootEvent != null)
        {
            ShootEvent();
        }
    }

    public void Update()
    {
        
    }

    public void FixedUpdate()
    {
        CheckRaycast();
    }

    public void CheckRaycast()
    {
        Camera camera = Camera.main;

        foreach (var rt in raycastPoints)
        {
            Ray ray = camera.ScreenPointToRay(rt.transform.position);
            aimRays.Add(ray);
        }

        bool objectInAim = false;
        IProjectile foundedHitObject = null;

        foreach (var ray in aimRays)
        {
            RaycastHit hitInfo;
            bool hit = Physics.Raycast(ray, out hitInfo, Mathf.Infinity, ~LayerMask.NameToLayer("Projectile"));

            if (hit)
            {
                objectInAim = true;
                if (hitInfo.collider.GetComponent<IProjectile>() != null)
                {
                    foundedHitObject = hitInfo.collider.GetComponent<IProjectile>();
                }
            }
        }

        aimRays.Clear();

        if (objectInAim)
        {
            if (trackingObject == null || trackingObject != foundedHitObject)
            {
                trackingTime = 0f;
                float projectileLifetime = foundedHitObject.GetLifeTime();
                int n = Mathf.Clamp(Mathf.RoundToInt(projectileLifetime), 0, 4);
                readyToShootTime = trackingToShootTime[n];
                StartTracking(readyToShootTime);
            }

            trackingObject = foundedHitObject;
            trackingTime += Time.deltaTime;

            float trackingValue = Mathf.InverseLerp(0, readyToShootTime, trackingTime);
            Tracking(trackingValue);
            if (trackingValue == 1f)
            {
                Shoot();
                trackingObject.Despawn();
            }
            else
            {
                if (trackingObject.GameObject().transform.position.y < 1.8f)
                {
                    Shoot();
                    int chance = UnityEngine.Random.Range(0, 4);
                    bool[] chancebool = new bool[4] { false, false, false, true };
                    if (chancebool[chance])
                    {
                        trackingObject.Despawn();
                    }
                }
            }
        }
        else
        {
            trackingObject = null;
            LostTracking(trackingTime);
            trackingTime = 0f;
        }
    }



    public void StartTracking(float time)
    {
        if (StartTrackingEvent != null)
        {
            StartTrackingEvent(time);
        }
    }

    public void Tracking(float value)
    {
        if(TrackingEvent != null)
        {
            TrackingEvent(value);
        }
    }

    public void LostTracking(float time)
    {
        if (LostTrackingEvent != null)
        {
            LostTrackingEvent(time);
        }
    }
}
