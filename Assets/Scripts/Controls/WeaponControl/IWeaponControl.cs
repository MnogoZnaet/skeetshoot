﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IWeaponControl 
{
    event Action ShootEvent;
    event Action ReloadEvent;
    event Action<float> StartTrackingEvent;
    event Action<float> TrackingEvent;
    event Action<float> LostTrackingEvent;

    void Shoot();
    void Reload();
    void StartTracking(float timeToShoot);
    void Tracking(float time);
    void LostTracking(float time);
}
