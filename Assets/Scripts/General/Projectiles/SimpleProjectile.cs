﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleProjectile : MonoBehaviour, IProjectile
{
    [SerializeField] private Rigidbody rigidbody;
    [SerializeField] private Collider collider;

    private float lifetime;

    public event Action DestroyedEvent;
    public event Action LaunchedEvent;

    public void Start()
    {
        lifetime = 0f;

        if (LaunchedEvent != null)
        {
            LaunchedEvent();
        }
    }

    public float GetLifeTime()
    {
        return lifetime;
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (DestroyedEvent != null)
        {
            DestroyedEvent();
        }
        this.GetComponent<DestroyMe>().enabled = true;
        this.gameObject.layer = 0;
    }

    public void SetExternalForce(Vector3 forceVector)
    {
        rigidbody.velocity = Vector3.zero;
        rigidbody.AddForce(forceVector);
    }

    public void SetPosition(Vector3 position)
    {

    }

    // Start is called before the first frame update
    void Awake()
    {
        rigidbody = this.gameObject.GetComponent<Rigidbody>();
        collider = this.gameObject.GetComponent<Collider>();
    }

    void Update()
    {
        lifetime += Time.deltaTime;
    }

    public GameObject GameObject()
    {
        return this.gameObject;
    }

    public void Despawn()
    {
        if (DestroyedEvent != null)
        {
            DestroyedEvent();
        }

        Destroy(this.gameObject);
    }
}
