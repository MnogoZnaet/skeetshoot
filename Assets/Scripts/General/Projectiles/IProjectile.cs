﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IProjectile 
{
    event Action DestroyedEvent;
    event Action LaunchedEvent;

    void SetPosition(Vector3 position);
    void SetExternalForce(Vector3 forceVector);
    void OnCollisionEnter(Collision collision);
    float GetLifeTime();
    GameObject GameObject();
    void Despawn();
}
