﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IProjectileActor 
{
    float FireRate { get; set; }
    IProjectile Fire();
    void SetFire(bool value);
}
