﻿using UnityEngine;
using System;

[Serializable]
public class ProjectileActorClip
{
    public string name;
    public GameObject projectile;
    public GameObject muzzleFlare;
    public float min, max;
}
