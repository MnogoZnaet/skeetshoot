﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ProjectileActor : MonoBehaviour, IProjectileActor
{
    private Vector3 aimTo;
    private bool firing;
    [SerializeField] private ProjectileActorClip clip;

    [SerializeField] private int[] randomRotation;

    public float FireRate { get; set; }
    public Transform spawnLocator;

    // Start is called before the first frame update
    void Start()
    {
        aimTo = spawnLocator.forward;
    }

    public void SetFire(bool value)
    {
        firing = value;
    }

    public IProjectile Fire()
    {
        GameObject newProjectile = GameObject.Instantiate(clip.projectile, spawnLocator.position, spawnLocator.rotation, null);
        IProjectile projectile = newProjectile.GetComponent<IProjectile>();

        int n = UnityEngine.Random.Range(0, randomRotation.Length);
        int randAngle = randomRotation[n];
        this.transform.localEulerAngles = new Vector3((float)randAngle, this.transform.localEulerAngles.y, this.transform.localEulerAngles.z);

        Vector3 shootVector = spawnLocator.forward;

        projectile.SetExternalForce(shootVector * UnityEngine.Random.Range(clip.min, clip.max));

        return projectile;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Fire();
        }
    }


}
