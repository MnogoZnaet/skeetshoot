﻿using System.Collections;
using System.Collections.Generic;
//using ReflexCLI.DefaultParameterProcessors;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class SimpleLocator : MonoBehaviour
{
    public bool showName;
    public bool showLocator;
    public Color locatorColor;

#if UNITY_EDITOR
    void OnDrawGizmos()
    {
        if (showName)
        {
            Handles.Label(this.transform.position + new Vector3(-0.05f, 0.2f, -0.05f), this.transform.name);
        }

        if (showLocator)
        {
            Gizmos.color = locatorColor;
            Gizmos.DrawWireSphere(this.transform.position, 0.05f);
        }
    }
#endif
}
