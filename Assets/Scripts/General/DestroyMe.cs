﻿using UnityEngine;
using System.Collections;

public class DestroyMe : MonoBehaviour
{

    [SerializeField] float timer;
    public float deathtimer = 10;
    public bool restoreScale;
    private Vector3 initialScale;

	// Use this for initialization
    void OnEnable()
    {
        timer = 0f;
        initialScale = this.transform.localScale;
    }

    void OnDisable()
    {
        timer = 0f;
        if (restoreScale)
        {
            this.transform.localScale = initialScale;
        }
    }

    // Update is called once per frame
    void Update ()
    {
        timer += Time.deltaTime;

        if(timer >= deathtimer)
        {
            Destroy(gameObject);
        }
	
	}

}
