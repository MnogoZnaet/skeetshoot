﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


// Main class that control all flow in the game
// Responsobilities: 
// - Load Main Menu
// - Loading Combat scene and providing it with requested information to start match
// - Unload Combat and passing combat result back to the main menu logic
public class FlowControl : MonoBehaviour
{
    #region Singleton
    private static FlowControl _instance;
    public static FlowControl Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<FlowControl>();

                if (_instance == null)
                {
                    Debug.LogError("FlowManager instance missing");
                }
            }

            return _instance;
        }
    }
    #endregion

    public string mainMenuScene = "MainMenu";
    public string combatScene = "CombatScene";

    public void Start()
    {
        RunGameLoadingProcess();
    }

    // Loading the first entry scene for player which is MainMenu
    public void RunGameLoadingProcess()
    {
        StartCoroutine(GameLoadingProcess());
    }

    // Load CombatScene
    public void RunGameStartProcess()
    {
        StartCoroutine(GameStartProcess());
    }

    // Unload combat scene, and go back to MainMenu scene
    public void RunGameOverProcess()
    {
        StartCoroutine(GameOverProcess());
    }

    private IEnumerator GameLoadingProcess()
    {
        // Async unload MainMenu
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(mainMenuScene, LoadSceneMode.Additive);
        asyncLoad.allowSceneActivation = false;

        while (!asyncLoad.isDone)
        {
            if (asyncLoad.progress >= 0.9f)
            {
                // we finally show the scene
                asyncLoad.allowSceneActivation = true;
            }
            yield return null;
        }

        // Set MainMenu scene as current
        SceneManager.SetActiveScene(SceneManager.GetSceneByName(mainMenuScene));

        yield return new WaitForEndOfFrame();
        yield break;
    }

    private IEnumerator GameStartProcess()
    {
        AsyncOperation asyncUnload, loadAsync;

        //Unload MainMenu scene
        asyncUnload = SceneManager.UnloadSceneAsync(mainMenuScene);
        while (!asyncUnload.isDone)
        {
            yield return null;
        }

        //Load CombatScene
        loadAsync = SceneManager.LoadSceneAsync(combatScene, LoadSceneMode.Additive);
        while (!loadAsync.isDone)
        {
            yield return null;
        }

        yield return new WaitForEndOfFrame();

        //Set CombatScene as current scene
        SceneManager.SetActiveScene(SceneManager.GetSceneByName(combatScene));
        yield break;
    }

    private IEnumerator GameOverProcess()
    {
        //Unload CombatScene 
        AsyncOperation asyncUnload = SceneManager.UnloadSceneAsync(combatScene);
        while (!asyncUnload.isDone)
        {
            yield return null;
        }

        yield return new WaitForEndOfFrame();

        //Load MainMenu scene
        AsyncOperation loadAsync = SceneManager.LoadSceneAsync(mainMenuScene, LoadSceneMode.Additive);
        while (!loadAsync.isDone)
        {
            yield return null;
        }

        yield return new WaitForSeconds(0f);

        //Set MainMenu scene active
        SceneManager.SetActiveScene(SceneManager.GetSceneByName(mainMenuScene));
        yield break;
    }
}